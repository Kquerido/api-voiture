FROM openjdk:12

COPY ./build/libs/cars-api.jar /app/cars-api.jar

WORKDIR /app

CMD ["java", "-jar", "cars-api.jar"]